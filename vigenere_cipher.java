public class vigenere_cipher {
	private static String alphabet="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()_+-=[] {};:'\"\\|><?,./";
	public static void main(String[] args) {
		String key1="m5ChWSIjDgexTX%H^!ev3k#Sk86e$ZT*5HoBR^rnmf3&@4iBv1#PjyWs9oVVFB&Y1GUbv4WOE5qRbd8^*iAQ3xVLR3IcqpQ";
		String key2="*eq3fwRoeqFkbu12YY1M^gA8cCMm81tmx^mr3WSoASG$VLIJMUf8Ja%HAGEIT#6ii*okLcCzuDOgwQ@Ji!c2$svGWN9G*vWs";
		String key3="RCRGIuClM$GvHXQgEDmpI@$YKCzRLtKwY$9%x6kva1E72k&s3P30y2H*4h4SAWAA0qJd$e0mxMqxI3!sDTvoIEBgPAK7gsO#V";
		String key4="MNTp^ST3DPsOyiXpz*bvUB2mS5n1yw8vkbG@eI7QdUewOQO4atlqkees6KqfcWlGI*TfnsbPU80lEvROUCUkqixziF97lJLo3K";
		String key5="OOfs5#3ZKblOvyoevXmBrweLH0sD32MkBPGyDmx1aCxzNAMjT1qTDPJ%h*7ceMd6J3@7JV*Zdxw2OstDHs^7HRY6TGWvv9Sd8t7";
		String message="Secret Message";
		
		System.out.println("Original: \n"+message+"\n");
		
		String[] arguments= {message, key1, key2, key3, key4, key5};
		String emessage=encrypt(arguments);
		System.out.println("Encrypted: \n"+emessage+"\n");
		
		String dmessage=decrypt(arguments);
		System.out.println("Decrypted: \n"+dmessage+"\n");
		
	}
	
	public static void setAlphabet(String a)
	{
		alphabet=a;
	}
	
	private static String ao(int o)
	{
		String output="";
		for (int x=0; x < alphabet.length(); x++)
			output+=alphabet.charAt((x+o)%alphabet.length()+ (((x+o)%alphabet.length()>=0) ? 0 : alphabet.length()));
		return output;
	}
	
	private static void check(String[] p)
	{
		char[] alpha = alphabet.toCharArray();
		String tempa="";
		for (char a : alpha)
			if(tempa.contains(""+a))
			{
				System.out.println("Your alphabet seems to conain duplicate characters. Please set an alphabet with only one of each character to ensure the encryption/decryption process works correctly.");
				break;
			} else
				tempa+=a;
		String temps="";
		for (String s : p)
			for (char y : s.toCharArray())
			{
				if (!alphabet.contains(""+y))
				{
					alphabet+=""+y;
					System.out.println("WARNING: your alphabet has been changed to "+alphabet);
				}
			}
	}
	
	public static String encrypt(String[] args, int n)
	{
		for (Object waste=null; n > 0; n--)
			args[0]=encrypt(args);
		for (Object waste=null; n < 0; n++)
			args[0]=decrypt(args);
		return args[0];
	}
	
	public static String decrypt(String[] args, int n)
	{
		for (Object waste=null; n > 0; n--)
			args[0]=decrypt(args);
		for (Object waste=null; n < 0; n++)
			args[0]=encrypt(args);
		return args[0];
	}
	
	public static String encrypt(String[] args)
	{
		if (args.length<=1)
			return args[0];
		check(args);
		String output="";
		for (int x=0; x < args[0].length(); x++)
		{
			String let="";
			for (int i =1; i <= args.length; i++)
			{
				let+=args[i%args.length].charAt(x%args[i%args.length].length());
			}
			output+=""+getLet(let);
		}
		return output;
	}
	
	private static char getLet(String s)
	{
		int offset=0;
		for (int x = 0; 0 < s.length()-1; s=s.substring(1))
		{
			offset+=x+alphabet.indexOf(s.charAt(0));
		}
		return alphabet.charAt((alphabet.indexOf(s.charAt(0))+offset)%alphabet.length());
	}
	
	public static String decrypt(String[] args)
	{
		if (args.length<=1)
			return args[0];
		check(args);
		String output="";
		for (int x=0; x < args[0].length(); x++)
		{
			String let="";
			for (int i =1; i <= args.length; i++)
			{
				let+=args[i%args.length].charAt(x%args[i%args.length].length());
			}
			output+=""+origLet(let);
		}
		return output;
	}
	
	private static char origLet(String s)
	{
		int offset=0;
		for (int x = 0; 0 < s.length()-1; s=s.substring(1))
		{
			offset+=x+alphabet.indexOf(s.charAt(0));
		}
		
		return ao(-offset).charAt(alphabet.indexOf(s.charAt(0)));
	}
}
