# vigenere_cipher

I expanded on the Vigenere Cipher to allow for unlimited encryption dimensions instead of just two (one being the message and one being the key). You can use two if you'd like but that's lame.

# How to use
The easiest way to use this code is to copy the code into [https://www.compilejava.net/](https://www.compilejava.net/), change the secret message and keys, and click the "COMPILE & EXECUTE" button. WARNING: you cannot use the " or the \ characters in current setup (copying and pasting strings in), due to the use of esape characters. If you actually call the program in another program, these characters are safe.

# Tips
The more random your keys are, the more secure they will be. It also helps to keep your keys different lengths than each other, but this is optional.

Additionally, you can add what I call "passes". The definition of a "pass" is how many times the program encrypts the message before printing it on the screen. This pass number has to be the same for encrypting and decrypting. To modify your pass number, find in my sample code where it says "encrypt(arguments)" and change it to "encrypt(arguments, #)" where # is the number of passes you want to make. You would change the decrypt command similarly.